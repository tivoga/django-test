import datetime
from django.db import models
from django.utils import timezone
from tags.models import Subject, Topic, Concept

class Problem(models.Model):
        question = models.CharField(max_length=200)
        create_date = models.DateTimeField('date created')
        concepts = models.ManyToManyField(Concept)

        def was_created_recently(self):
            now = timezone.now()
            return now - datetime.timedelta(days=1) <= self.create_date < now
        was_created_recently.admin_order_field = 'create_date'
        was_created_recently.boolean = True
        was_created_recently.short_description = 'Created recently?'
        
        def concepts_names(self):
            return ', '.join([a.title for a in self.concepts.all()])
        concepts_names.short_description = "Concepts"
        
        def get_topic(self):
            return ', '.join([a.topic.title for a in self.concepts.all()])
        get_topic.short_description = 'Topic'
        
        def get_subject(self):
            return self.concepts.all()[0].topic.subject.title
            #', '.join([a.topic.subject.title for a in self.concepts.all()])
        get_subject.short_description = 'Subject'

        def __unicode__(self):
                return self.question

class Choice(models.Model):
        problem = models.ForeignKey(Problem)
        choice_text = models.CharField(max_length=200)
        votes = models.IntegerField(default=0)

        def __unicode__(self):
                return self.choice_text


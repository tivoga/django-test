from django.contrib import admin
from problems.models import Problem, Choice
from tags.models import Subject, Topic, Concept

class ChoiceInline(admin.TabularInline):
	model = Choice
	extra = 3

class ProblemAdmin(admin.ModelAdmin):
	# order the fields
	# fields = ['create_date', 'question']

	fieldsets = [
		(None, 				 {'fields':['question','concepts']}),
		('Date information', {'fields':['create_date'], 'classes':['collapse']}),
	]
	inlines = [ChoiceInline]
	list_display = ('question','concepts_names', 'get_topic', 'get_subject', 'create_date', 'was_created_recently')
	list_filter = ['create_date', 'concepts', 'concepts__topic', 'concepts__topic__subject']
	search_fields = ['question']
	date_hierarchy = 'create_date'
	
admin.site.register(Problem, ProblemAdmin)


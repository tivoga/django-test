from django.contrib import admin
from tags.models import Subject, Topic, Concept


admin.site.register(Subject)
admin.site.register(Topic)
admin.site.register(Concept)

#class ChoiceInline(admin.TabularInline):
#    model = Choice
#    extra = 3
#
#class SubjectAdmin(admin.ModelAdmin):
#    # order the fields
#    # fields = ['create_date', 'question']
#
#    fieldsets = [
#        (None,                  {'fields':['question']}),
#        ('Date information', {'fields':['create_date'], 'classes':['collapse']}),
#    ]
#    inlines = [ChoiceInline]
#    list_display = ('question', 'create_date', 'was_created_recently')
#    list_filter = ['create_date']
#    search_fields = ['question']
#    date_hierarchy = 'create_date'

#admin.site.register(Subject, SubjectAdmin)

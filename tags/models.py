from django.db import models

# Create your models here.

class Subject(models.Model):
        title = models.CharField(max_length=200)
        create_date = models.DateTimeField('date created')
        
        def __unicode__(self):
                return self.title
        
class Topic(models.Model):
        title = models.CharField(max_length=200)
        create_date = models.DateTimeField('date created')
        subject = models.ForeignKey(Subject)
        
        def __unicode__(self):
                return self.title
        
class Concept(models.Model):
        title = models.CharField(max_length=200)
        create_date = models.DateTimeField('date created')
        topic = models.ForeignKey(Topic)
        
        def __unicode__(self):
                return self.title